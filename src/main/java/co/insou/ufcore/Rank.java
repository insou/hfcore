package co.insou.ufcore;

/**
 * Created by insou on 24/12/2015.
 */
public enum Rank {

    MEMBER("Member", 0),
    ARTISAN("Artisan", 1),
    MERCHANT("Merchant", 2),
    SCRIBE("Scribe", 3),
    NOBLE("Noble", 4),
    PHAROH("Pharoh", 5);

    private int index;
    private String name;

    Rank(String name, int index) {
        this.index = index;
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public String getName() {
        return name;
    }

    public static Rank getRank(String name) {
        switch (name.toLowerCase()) {
            case "member":
                return MEMBER;
            case "artisan":
                return ARTISAN;
            case "merchant":
                return MERCHANT;
            case "scribe":
                return SCRIBE;
            case "noble":
                return NOBLE;
            case "pharoh":
                return PHAROH;
            default:
                return null;
        }
    }

    public static Rank getRank(int index) {
        switch (index) {
            case 0:
                return MEMBER;
            case 1:
                return ARTISAN;
            case 2:
                return MERCHANT;
            case 3:
                return SCRIBE;
            case 4:
                return NOBLE;
            case 5:
                return PHAROH;
            default:
                return null;
        }
    }

}
