package co.insou.ufcore;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.bukkit.Bukkit;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

/**
 * Created by insou on 23/12/2015.
 */
public class Database {

    private final Main plugin;

    private HikariDataSource dataSource;

    private String SQL_URL;
    private String username;
    private String password;

    private int minConnections;
    private int maxConnections;
    private long connectionTimeout;

    protected Database(Main plugin) {
        this.plugin = plugin;
        init();
    }

    private void init() {
        HikariConfig config = new HikariConfig();
        setSQLURL(
                "jdbc:mysql://" + plugin.getUFConfig().getSqlHostname() + ":" + plugin.getUFConfig().getSqlPort() + "/" + plugin.getUFConfig().getSqlDatabase(),
                plugin.getUFConfig().getSqlUsername(),
                plugin.getUFConfig().getSqlPassword()
        );
        setConnectionPoolOptions(
                plugin.getUFConfig().getInitSize(),
                plugin.getUFConfig().getMaxActive(),
                plugin.getUFConfig().getMaxWait()
        );
        setupDataSource();
        makeTables();
    }

    public void setupDataSource() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(SQL_URL);
        config.setDriverClassName("com.mysql.jdbc.Driver");
        config.setUsername(username);
        config.setPassword(password);
        config.setMinimumIdle(minConnections);
        config.setMaximumPoolSize(maxConnections);
        config.setConnectionTimeout(connectionTimeout);
        dataSource = new HikariDataSource(config);
    }

    private void makeTables() {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = getConnection();
            ps = conn.prepareStatement(
                            "CREATE TABLE IF NOT EXISTS `UFCore` (" +
                            "UUID VARCHAR(30), " +
                            "Rank INT, " +
                            "PRIMARY KEY(UUID)" +
                            ");"
            );
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            plugin.getLogger().log(Level.SEVERE, "Error connecting to SQL! Disabling UFCore.");
            Bukkit.getPluginManager().disablePlugin(plugin);
        }
    }

    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    private void setSQLURL(String SQLURL, String username, String password) {
        this.SQL_URL = SQLURL;
        this.username = username;
        this.password = password;
    }

    private void setConnectionPoolOptions(int minConnections, int maxConnections, long connectionTimeout) {
        this.minConnections = minConnections;
        this.maxConnections = maxConnections;
        this.connectionTimeout = connectionTimeout;
    }

    public void close(Connection conn, PreparedStatement ps, ResultSet res) {
        if (conn != null) try {
            conn.close();
        } catch (SQLException ignored) {
        }
        if (ps != null) try {
            ps.close();
        } catch (SQLException ignored) {
        }
        if (res != null) try {
            res.close();
        } catch (SQLException ignored) {
        }
    }


}
