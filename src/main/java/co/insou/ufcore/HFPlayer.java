package co.insou.ufcore;

import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by insou on 24/12/2015.
 */
public class HFPlayer {

    private static Map<Player, HFPlayer> players = new HashMap<>();

    public static void addPlayer(Player player, HFPlayer hfPlayer) {
        players.put(player, hfPlayer);
    }

    public static void removePlayer(Player player) {
        players.remove(player);
    }

    public static HFPlayer getPlayer(Player player) {
        return players.get(player);
    }

    public static void clear() {
        players.clear();
    }

    private final Player player;

    private Rank rank;

    protected HFPlayer(Player player, Rank rank) {
        this.player = player;
        this.rank = rank;
    }

    public Player getPlayer() {
        return player;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

}
