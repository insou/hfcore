package co.insou.ufcore;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by insou on 23/12/2015.
 */
public class Main extends JavaPlugin implements Listener {

    private Config config;
    private Database database;

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
        config = new Config(this);
        database = new Database(this);
    }

    @Override
    public void onDisable() {
        HFPlayer.clear();
    }

    public Config getUFConfig() {
        return config;
    }

    @EventHandler
    public void onPlayerLogin(PlayerLoginEvent e) {
        Player player = e.getPlayer();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        try {
            conn = database.getConnection();
            ps = conn.prepareStatement(
                    "SELECT Rank FROM `UFCore` WHERE UUID=?"
            );
            ps.setString(1, player.getUniqueId().toString());
            res = ps.executeQuery();
            if (!res.next()) {
                ps = conn.prepareStatement("INSERT INTO `UFCore` VALUES (?, ?)");
                ps.setString(1, player.getUniqueId().toString());
                ps.setInt(2, 0);
                ps.executeUpdate();
                HFPlayer.addPlayer(player, new HFPlayer(player, Rank.MEMBER));
            } else {
                int rankIndex = res.getInt("Rank");
                Rank rank = Rank.getRank(rankIndex);
                if (rank == null) {
                    getLogger().severe("SQL PROBLEM onPlayerJoin - RANK IS NULL. Contact insou on Skype 'crucial.mc'");
                    e.disallow(PlayerLoginEvent.Result.KICK_OTHER, "Could not retrieve profile from SQL. Contact an Admin!");
                    return;
                }
                HFPlayer.addPlayer(player, new HFPlayer(player, rank));
            }
        } catch (SQLException exception) {
            getLogger().severe("SQLException onPlayerJoin - Check SQL information / Connections");
            e.disallow(PlayerLoginEvent.Result.KICK_OTHER, "Could not retrieve profile from SQL. Contact an Admin!");
            exception.printStackTrace();
        } finally {
            database.close(conn, ps, res);
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        HFPlayer.removePlayer(e.getPlayer());
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("setrank")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "You must be a Player to run this!");
                return false;
            }
            Player player = (Player) sender;
            if (!player.hasPermission("ufcore.setrank")) {
                player.sendMessage(ChatColor.RED + "You don't have permission to do this!");
                return false;
            }
            if (args.length != 2) {
                player.sendMessage(ChatColor.RED + "/" + label + " <Player> <Rank>");
                return false;
            }
            Player toSet = Bukkit.getPlayer(args[0]);
            if (toSet == null) {
                player.sendMessage(ChatColor.RED + args[0] + " is not online!");
                return false;
            }
            Integer rankID;
            Rank rank;
            try {
                rankID = Integer.parseInt(args[1]);
                rank = Rank.getRank(rankID);
                if (rank == null) {
                    throw new NumberFormatException();
                }
            } catch (NumberFormatException e) {
                try {
                    rank = Rank.getRank(args[1]);
                    if (rank == null) {
                        throw new IllegalArgumentException();
                    }
                } catch (IllegalArgumentException e1) {
                    player.sendMessage(ChatColor.RED + args[1] + " didn't identify as a Rank!");
                    return false;
                }
            }
            HFPlayer hfPlayer = HFPlayer.getPlayer(toSet);
            if (hfPlayer == null) {
                player.sendMessage(ChatColor.RED + "Internal Error: HFPlayer is null, Player is not on system.");
                return false;
            }
            Connection conn = null;
            PreparedStatement ps = null;
            try {
                conn = database.getConnection();
                ps = conn.prepareStatement(
                        "UPDATE `HFCore Rank=? WHERE UUID=?"
                );
                ps.setInt(1, rank.getIndex());
                ps.setString(2, toSet.getUniqueId().toString());
                ps.executeUpdate();
                hfPlayer.setRank(rank);
            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
            player.sendMessage(ChatColor.GREEN + "Set " + toSet.getName() + "'s rank to " + hfPlayer.getRank());
        }
        return false;
    }

}
