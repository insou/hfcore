package co.insou.ufcore;

import org.bukkit.configuration.file.FileConfiguration;

/**
 * Created by insou on 23/12/2015.
 */
public class Config {

    private final Main plugin;
    private FileConfiguration conf;

    private String sqlHostname;
    private String sqlPort;
    private String sqlDatabase;
    private String sqlUsername;
    private String sqlPassword;
    private int initSize;
    private int maxActive;
    private int maxWait;
    private int maxIdle;


    protected Config(Main plugin) {
        this.plugin = plugin;
        conf = plugin.getConfig();
        init();
    }

    private void init() {
        plugin.saveDefaultConfig();
        sqlHostname = conf.getString("sql.hostname");
        sqlPort = conf.getString("sql.port");
        sqlDatabase = conf.getString("sql.database");
        sqlUsername = conf.getString("sql.username");
        sqlPassword = conf.getString("sql.password");
        initSize = conf.getInt("sql.connection-pool.initial-size");
        maxActive = conf.getInt("sql.connection-pool.max-active");
        maxWait = conf.getInt("sql.connection-pool.max-wait");
        maxIdle = conf.getInt("sql.connection-pool.max-idle");
    }

    public String getSqlHostname() {
        return sqlHostname;
    }

    public String getSqlPort() {
        return sqlPort;
    }

    public String getSqlDatabase() {
        return sqlDatabase;
    }

    public String getSqlUsername() {
        return sqlUsername;
    }

    public String getSqlPassword() {
        return sqlPassword;
    }

    public int getInitSize() {
        return initSize;
    }

    public int getMaxActive() {
        return maxActive;
    }

    public int getMaxWait() {
        return maxWait;
    }

    public int getMaxIdle() {
        return maxIdle;
    }
}
